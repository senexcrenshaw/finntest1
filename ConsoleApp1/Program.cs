﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp1
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                var dts = File.ReadAllLines(@"..\meetinginforeport.csv").ToList().ToDT();
                var ccs = dts.CountOverLaps();

                using (var file =
                    new StreamWriter(@"out.csv"))
                {
                    foreach (var c in ccs)
                    {
                        file.WriteLine($"{c.Key},{c.Value}");
                        Console.WriteLine($"{c.Key} : {c.Value}");
                    }
                }
                Console.WriteLine($"Max count: {ccs.Values.Max()}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    /// <summary>
    ///     Simple class
    /// </summary>
    public class DT
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool Checked { get; set; }
    }

    public static class Extensions
    {
        /// <summary>
        ///     Extensions method to look for overlaps
        /// </summary>
        /// <param name="dts"></param>
        /// <returns></returns>
        public static Dictionary<DateTime, int> CountOverLaps(this List<DT> dts)
        {
            var overLapCount = 0;
            var ret = new Dictionary<DateTime, int>();

            foreach (var dt in dts)
            {
                dt.Checked = true; // Mark it checked

                //Get a list of dates that have a start date that is before this end date (overlaps)
                //the list is in start order so just the end matters.
                var found = dts.Where(a => !a.Checked && dt.End > a.Start).ToList();

                //Didnt find any, move on
                if (!found.Any())
                    continue;

                //Add in the count into our return value
                if (found.Count > overLapCount)
                    overLapCount = found.Count;

                ret[dt.Start] = found.Count;
            }

            //All done - return
            return ret;
        }


        /// <summary>
        ///     Simple extension with some simple datetime checks. It will ignore any poorly formatted dates.. This includes
        ///     headers
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static List<DT> ToDT(this List<string> lines)
        {
            var ret = new List<DT>();
            foreach (var line in lines)
            {
                var s = line.Split(",");
                if (!DateTime.TryParse(s[3], out DateTime st))
                    continue;
                if (!DateTime.TryParse(s[5], out DateTime et))
                    continue;
                ret.Add(new DT {Start = st, End = et});
            }
            return ret.OrderBy(x => x.Start).ToList();
        }
    }
}