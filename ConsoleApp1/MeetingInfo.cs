﻿using System;

namespace ConsoleApp1
{
    public class MeetingInfo
    {
        // Scheduler Name, Meeting ID,Meeting Subject, Date held,Billing Code, End Time,Region Name, Site Name,Node Name, Actual Length
        public string SchedulerName { get; set; }

        public int Id { get; set; }
        public string MeetingSubject { get; set; }
        public DateTime DateHeld { get; set; }
        public string Billing { get; set; }
        public DateTime EndTIme { get; set; }
        public string RegionName { get; set; }
        public string SiteName { get; set; }
        public string NodeName { get; set; }
        public int ActualLength { get; set; }
    }
}