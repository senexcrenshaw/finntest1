import csv
from datetime import datetime


FILENAME = 'meetinginforeport.csv'


def key(item):
    return item[0]


num_conferences = 0
max_conferences = 0
dates = []

with open(FILENAME) as f:
    reader = csv.DictReader(f)
    next(reader)  # Skip second row, contains type fields
    for row in reader:
        start = (datetime.strptime(row['Date held'], '%m/%d/%Y %H:%M'), 'start')
        end = (datetime.strptime(row['End Time'], '%m/%d/%Y %H:%M'), 'end')
        dates.extend((start, end))
        dates.sort(key=key)

for date in dates:
    if date[1] == 'start':
        num_conferences += 1
        print(date[1], num_conferences)
    else:
        num_conferences -= 1
        print(date[1], num_conferences)

    if num_conferences > max_conferences:
        max_conferences = num_conferences


print('Number of concurrent conferences = {}'.format(max_conferences))


